﻿using System;
using System.Collections.Generic;
using System.Text;
using data_grid_minimal.Models;

namespace data_grid_minimal.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public List<Measurement> Data { get; set; }
        public RoundingConfiguration RoundingConfiguration { get; init; }

        public MainWindowViewModel()
        {
            RoundingConfiguration = new RoundingConfiguration() { D = 0.02f };

            var list = new List<Measurement>();
            for (int i = 0; i < 5; i++)
            {
                float val = 1.0f + 0.01f * i;
                list.Add(new Measurement() { Width = val, Length = val, Height = val});
            }
            Data = list;
        }
    }
}
