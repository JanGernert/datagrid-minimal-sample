using System;
using System.Collections.Generic;
using System.Text;
using ReactiveUI;

namespace data_grid_minimal.Models
{
    public class RoundingConfiguration
    {
        public float D { get; init; }
    }
}
