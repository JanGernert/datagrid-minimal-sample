using System;
using System.Collections.Generic;
using System.Text;
using ReactiveUI;

namespace data_grid_minimal.Models
{
    public class Measurement
    {
        public float Width { get; init; }
        public float Length { get; init; }
        public float Height { get; init; }
    }
}
