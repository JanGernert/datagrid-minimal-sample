using System;
using System.Collections.Generic;
using System.Globalization;
using Avalonia.Data.Converters;
using data_grid_minimal.Models;

namespace data_grid_minimal
{
    public sealed class MeasurementConverter : IMultiValueConverter
    {
        public object Convert(IList<object> values, Type targetType, object parameter, CultureInfo culture)
        {
            float dimension = (float)values[0];
            RoundingConfiguration roundingConfiguration = values[1] as RoundingConfiguration;
            uint decimals = 2;
            return dimension.ToString($"N{decimals}", CultureInfo.InvariantCulture);
        }
    }
}